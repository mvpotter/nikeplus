import sys
import os

dir = os.path.dirname(__file__)
model_dir = os.path.join(dir, '../model')

sys.path.append(model_dir)

class CsvConverter() :
    def activity_to_csv(self, activity) :
        values_array = []
        values_array.append(activity.id)
        values_array.append(activity.type)
        values_array.append(activity.startTime.strftime('%Y%m%d%H%m%S'))
        values_array.append(activity.deviceType)
        values_array.append(activity.metricSummary.calories)
        values_array.append(activity.metricSummary.fuel)
        values_array.append(activity.metricSummary.distance)
        values_array.append(activity.metricSummary.steps)
        values_array.append(activity.metricSummary.duration.strftime('%H%M%S'))
        values_array.append(len(activity.tags))
        for key in activity.tags.keys() :
            values_array.append('{}={}'.format(key, activity.tags[key]))
        csv_string = ', '.join(str(v) for v in values_array)
        return csv_string
    
    def activity_list_to_csv(self, activity_list) :
        csv_strings_array = []
        for activity in activity_list :
            csv_strings_array.append(self.activity_to_csv(activity))
        csv = '\n'.join(csv_strings_array)
        return csv

    def to_csv(self, activity_list, destination_file) :
        with open(destination_file, 'w') as file :
            file.write(self.activity_list_to_csv(activity_list))
        print ('done')
        
