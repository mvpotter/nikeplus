class MetricSummary():
    def __repr__(self):
        return ('calories = {}, fuel = {}, distance = {}, steps = {}, duration = {}'.format(
                    self.calories, self.fuel, self.distance, self.steps, self.duration
                ))
