class Activity:
	
	def __repr__(self):
		return 'id = {0}, type = {1}, distance = {2}'.format(
                            self.id, self.type, self.metricSummary.distance
                        )
