import sys
import os

dir = os.path.dirname(__file__)
lib_dir = os.path.join(dir, '../../lib')
model_dir = os.path.join(dir, '../model')

sys.path.append(lib_dir)
sys.path.append(model_dir)
import json
from activity import Activity
from metricsummary import MetricSummary
import dateutil.parser
from dateutil import tz

class ActivityParser :

	JSON_DATA = 'data'
	
	JSON_ACTIVITY_ID = "activityId"
	JSON_ACTIVITY_TYPE = "activityType"
	JSON_START_TIME = 'startTime'
	JSON_TIME_ZONE = 'activityTimeZone'
	JSON_DEVICE_TYPE = 'deviceType'
	
	JSON_METRIC_SUMMARY = 'metricSummary'
	JSON_CALORIES = 'calories'
	JSON_FUEL = 'fuel'
	JSON_DISTANCE = 'distance'
	JSON_STEPS = 'steps'
	JSON_DURATION = 'duration'
	
	JSON_TAGS = 'tags'
	JSON_TAG_TYPE = 'tagType'
	JSON_TAG_VALUE = 'tagValue'

	JSON_METRICS = 'metrics'
	JSON_INTERVAL_METRIC = 'intervalMetric'
	JSON_INTERVLAL_UNIT= 'intervalUnit'
	JSON_METRIC_TYPE = 'metricType'
	JSON_VALUES = 'values'

	def parse_activity_list(self, json_data) :
		data = json.loads(json_data)
		activities = []
		for activity_json in data[self.JSON_DATA]:
			activities.append(self.parse_activity(json.dumps(activity_json)))
			
		return activities 
	
	def parse_activity(self, json_data) :
		activity_json = json.loads(json_data)
		activity = Activity()
			
		activity.id = activity_json[self.JSON_ACTIVITY_ID]
		activity.type = activity_json[self.JSON_ACTIVITY_TYPE]
		activity.startTime = self.parse_datetime(activity_json[self.JSON_START_TIME], 
												 activity_json[self.JSON_TIME_ZONE])
		activity.deviceType = activity_json[self.JSON_DEVICE_TYPE]
                
		metric_summary = MetricSummary()
		metric_summary_json = activity_json[self.JSON_METRIC_SUMMARY]
		
		metric_summary.calories = metric_summary_json[self.JSON_CALORIES]
		metric_summary.fuel = metric_summary_json[self.JSON_FUEL]
		metric_summary.distance = metric_summary_json[self.JSON_DISTANCE]
		metric_summary.steps = metric_summary_json[self.JSON_STEPS]
		metric_summary.duration = dateutil.parser.parse(metric_summary_json[self.JSON_DURATION])
		activity.metricSummary = metric_summary;
                
		tags = {}
		for tag in activity_json[self.JSON_TAGS]:
			tags[tag[self.JSON_TAG_TYPE]] = tag[self.JSON_TAG_VALUE]
		activity.tags = tags
			
		return activity	
	
	def parse_datetime(self, utc_datetime_string, timezone_string) :
		from_zone = tz.tzutc()
		to_zone = tz.gettz(timezone_string)	
	
		utc = dateutil.parser.parse(utc_datetime_string)
	
		# Tell the datetime object that it's in UTC time zone since 
		# datetime objects are 'naive' by default
		utc = utc.replace(tzinfo = from_zone)

		# Convert time zone
		return utc.astimezone(to_zone)
