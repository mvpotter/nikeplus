Usage:
======

1. Activity list:
-----------------

```python
from nikeplus_parser import ActivityParser
parser = ActivityParser()
activities = parser.parse_activity_list(json_string)
```

Where json_string is like:

```json
{
  "data": [
    {
      "activityId": "8209000000002643737120000721557175414307",
      "activityType": "RUN",
      "startTime": "2013-09-17T14:09:50Z",
      "activityTimeZone": "Asia/Bangkok",
      "status": "COMPLETE",
      "deviceType": "IPOD",
      "metricSummary": {
        "calories": 616,
        "fuel": 2201,
        "distance": 10.001099586486816,
        "steps": 7805,
        "duration": "0:48:07.739"
      },
      "tags": [],
      "metrics": []
    },
    {
      "activityId": "42518131857",
      "activityType": "RUN",
      "startTime": "2013-09-12T13:34:09Z",
      "activityTimeZone": "Asia/Bangkok",
      "status": "COMPLETE",
      "deviceType": "IPOD",
      "metricSummary": {
        "calories": 332,
        "fuel": 1185,
        "distance": 5.383299827575684,
        "steps": 4398,
        "duration": "0:26:46.203"
      },
      "tags": [],
      "metrics": []
    }
  ]
}
```

2. Activity
-----------

```python
from nikeplus_parser import ActivityParser
parser = ActivityParser()
activity = parser.parse_activity(json_string)
```

Where json_string is like:

```json
{
	"activityId": "42518138307",
	"activityType": "RUN",
	"startTime": "2013-09-11T13:17:36Z",
	"activityTimeZone": "Asia/Bangkok",
	"status": "COMPLETE",
	"deviceType": "IPOD",
	"metricSummary": {
	"calories": 456,
	"fuel": 1622,
	"distance": 7.411099910736084,
	"steps": 5486,
	"duration": "0:33:41.013"
	},
	"tags": [],
	"metrics": []
}
```