import sys
import os

dir = os.path.dirname(__file__)
parser_dir = os.path.join(dir, '../parser')
converter_dir = os.path.join(dir, '../converter')
data_dir = os.path.join(dir, '../../data')

sys.path.append(parser_dir)
sys.path.append(converter_dir)
from nikeplus_parser import ActivityParser
from csv_converter import CsvConverter

activity_parser = ActivityParser()
json_file = open(os.path.join(data_dir, 'michael_potter.json')) 
activities = activity_parser.parse_activity_list(json_file.read())

converter = CsvConverter()
converter.to_csv(activities, os.path.join(data_dir, 'michael_potter.csv'))